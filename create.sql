-- Table: Administradores
CREATE TABLE Administradores (
    trabajador_id int  NOT NULL,
    turno varchar(40)  NOT NULL,
    CONSTRAINT Administradores_pk PRIMARY KEY (trabajador_id)
);



-- Table: Agenda_Clases
CREATE TABLE Agenda_Clases (
    sala_id int  NOT NULL,
    clase_id int  NOT NULL,
    dia smallint  NOT NULL,
    hora_inicio smallint  NOT NULL,
    hora_fin smallint  NOT NULL,
    CONSTRAINT Agenda_Clases_pk PRIMARY KEY (sala_id,clase_id)
);



-- Table: Agenda_Limpieza
CREATE TABLE Agenda_Limpieza (
    trabajador_id int  NOT NULL,
    sala_id int  NOT NULL,
    dia_semana smallint  NOT NULL,
    CONSTRAINT Agenda_Limpieza_pk PRIMARY KEY (trabajador_id,sala_id,dia_semana)
);



-- Table: Clases
CREATE TABLE Clases (
    tipo varchar(20)  NOT NULL,
    matriculados int  NOT NULL,
    min int  NOT NULL,
    max int  NOT NULL,
    nombre varchar(20)  NOT NULL,
    clase_id int  NOT NULL,
    especialidad varchar(100)  NOT NULL,
    CONSTRAINT Clases_pk PRIMARY KEY (clase_id)
);



-- Table: Clientes
CREATE TABLE Clientes (
    dni varchar(9)  NOT NULL,
    nombre varchar(100)  NOT NULL,
    apellidos varchar(100)  NOT NULL,
    telefono int  NOT NULL,
    email varchar(100)  NOT NULL,
    fecha_alta date  NOT NULL,
    cuenta_bancaria varchar(24)  NOT NULL,
    moroso boolean  NOT NULL,
    poblacion varchar(200)  NOT NULL,
    cp int  NOT NULL,
    calle varchar(100)  NOT NULL,
    cliente_id int  NOT NULL,
    fecha_baja date  NULL,
    id_admin_baja int  NULL,
    user_id varchar(20)  NOT NULL,
    CONSTRAINT Clientes_User_Id UNIQUE (user_id) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT Clientes_pk PRIMARY KEY (cliente_id)
);



-- Table: Formacion_Profesores
CREATE TABLE Formacion_Profesores (
    trabajador_id int  NOT NULL,
    titulo_id int  NOT NULL,
    CONSTRAINT Formacion_Profesores_pk PRIMARY KEY (trabajador_id,titulo_id)
);



-- Table: Imparte_Clases
CREATE TABLE Imparte_Clases (
    trabajador_id int  NOT NULL,
    clase_id int  NOT NULL,
    CONSTRAINT Imparte_Clases_pk PRIMARY KEY (trabajador_id,clase_id)
);



-- Table: Inscripciones
CREATE TABLE Inscripciones (
    cliente_id int  NOT NULL,
    clase_id int  NOT NULL,
    fecha date  NOT NULL,
    CONSTRAINT Inscripciones_pk PRIMARY KEY (cliente_id,clase_id)
);



-- Table: Limpiadores
CREATE TABLE Limpiadores (
    trabajador_id int  NOT NULL,
    CONSTRAINT Limpiadores_pk PRIMARY KEY (trabajador_id)
);



-- Table: Profesores
CREATE TABLE Profesores (
    trabajador_id int  NOT NULL,
    horas_semana int  NOT NULL,
    CONSTRAINT Profesores_pk PRIMARY KEY (trabajador_id)
);



-- Table: Salas
CREATE TABLE Salas (
    sala_id int  NOT NULL,
    nombre varchar(100)  NOT NULL,
    tipo varchar(20)  NOT NULL,
    capacidad int  NOT NULL,
    CONSTRAINT Salas_pk PRIMARY KEY (sala_id)
);



-- Table: Titulos
CREATE TABLE Titulos (
    titulo_id int  NOT NULL,
    nombre varchar(100)  NOT NULL,
    descripcion text  NOT NULL,
    CONSTRAINT Titulos_pk PRIMARY KEY (titulo_id)
);



-- Table: Trabajadores
CREATE TABLE Trabajadores (
    dni varchar(9)  NOT NULL,
    nombre varchar(100)  NOT NULL,
    apellidos varchar(100)  NOT NULL,
    telefono int  NOT NULL,
    email varchar(100)  NOT NULL,
    calle varchar(200)  NOT NULL,
    cp int  NOT NULL,
    poblacion varchar(200)  NOT NULL,
    trabajador_id int  NOT NULL,
    user_id varchar(20)  NOT NULL,
    CONSTRAINT Trabajadores_user_id UNIQUE (user_id) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT Trabajadores_pk PRIMARY KEY (trabajador_id)
);







-- foreign keys
-- Reference:  Agenda_Clase (table: Agenda_Clases)

ALTER TABLE Agenda_Clases ADD CONSTRAINT Agenda_Clase 
    FOREIGN KEY (clase_id)
    REFERENCES Clases (clase_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Agenda_Limpieza_Limpiador (table: Agenda_Limpieza)

ALTER TABLE Agenda_Limpieza ADD CONSTRAINT Agenda_Limpieza_Limpiador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Limpiadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  RESTRICT 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Agenda_Limpieza_Sala (table: Agenda_Limpieza)

ALTER TABLE Agenda_Limpieza ADD CONSTRAINT Agenda_Limpieza_Sala 
    FOREIGN KEY (sala_id)
    REFERENCES Salas (sala_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Agenda_Sala (table: Agenda_Clases)

ALTER TABLE Agenda_Clases ADD CONSTRAINT Agenda_Sala 
    FOREIGN KEY (sala_id)
    REFERENCES Salas (sala_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Cliente_Inscripciones (table: Inscripciones)

ALTER TABLE Inscripciones ADD CONSTRAINT Cliente_Inscripciones 
    FOREIGN KEY (cliente_id)
    REFERENCES Clientes (cliente_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Formacion_Profesor_Profesor (table: Formacion_Profesores)

ALTER TABLE Formacion_Profesores ADD CONSTRAINT Formacion_Profesor_Profesor 
    FOREIGN KEY (trabajador_id)
    REFERENCES Profesores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Imparte_Clase_Clase (table: Imparte_Clases)

ALTER TABLE Imparte_Clases ADD CONSTRAINT Imparte_Clase_Clase 
    FOREIGN KEY (clase_id)
    REFERENCES Clases (clase_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Imparte_Clase_Profesor (table: Imparte_Clases)

ALTER TABLE Imparte_Clases ADD CONSTRAINT Imparte_Clase_Profesor 
    FOREIGN KEY (trabajador_id)
    REFERENCES Profesores (trabajador_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Inscripciones_Clase (table: Inscripciones)

ALTER TABLE Inscripciones ADD CONSTRAINT Inscripciones_Clase 
    FOREIGN KEY (clase_id)
    REFERENCES Clases (clase_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Limpiador_Trabajador (table: Limpiadores)

ALTER TABLE Limpiadores ADD CONSTRAINT Limpiador_Trabajador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Trabajadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Profesor_Trabajador (table: Profesores)

ALTER TABLE Profesores ADD CONSTRAINT Profesor_Trabajador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Trabajadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Table_13_Trabajador (table: Administradores)

ALTER TABLE Administradores ADD CONSTRAINT Table_13_Trabajador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Trabajadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Titulo_Formacion_Profesor (table: Formacion_Profesores)

ALTER TABLE Formacion_Profesores ADD CONSTRAINT Titulo_Formacion_Profesor 
    FOREIGN KEY (titulo_id)
    REFERENCES Titulos (titulo_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;
