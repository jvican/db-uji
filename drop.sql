-- Foreign Keys
ALTER TABLE Agenda_Clases DROP CONSTRAINT Agenda_Clase;

ALTER TABLE Agenda_Limpieza DROP CONSTRAINT Agenda_Limpieza_Limpiador;

ALTER TABLE Agenda_Limpieza DROP CONSTRAINT Agenda_Limpieza_Sala;

ALTER TABLE Agenda_Clases DROP CONSTRAINT Agenda_Sala;

ALTER TABLE Inscripciones DROP CONSTRAINT Cliente_Inscripciones;

ALTER TABLE Formacion_Profesores DROP CONSTRAINT Formacion_Profesor_Profesor;

ALTER TABLE Imparte_Clases DROP CONSTRAINT Imparte_Clase_Clase;

ALTER TABLE Imparte_Clases DROP CONSTRAINT Imparte_Clase_Profesor;

ALTER TABLE Inscripciones DROP CONSTRAINT Inscripciones_Clase;

ALTER TABLE Limpiadores DROP CONSTRAINT Limpiador_Trabajador;

ALTER TABLE Profesores DROP CONSTRAINT Profesor_Trabajador;

ALTER TABLE Administradores DROP CONSTRAINT Table_13_Trabajador;

ALTER TABLE Formacion_Profesores DROP CONSTRAINT Titulo_Formacion_Profesor;

DROP VIEW MIS_CLASES_DE_HOY;
DROP VIEW PLANIFICACION_SEMANAL_LIMPIEZA;
DROP VIEW ALUMNOS_CLASE_HOY;
DROP VIEW ALUMNOS_DE_KARATE;

DROP TABLE Administradores;
DROP TABLE Agenda_Clases;
DROP TABLE Agenda_Limpieza;
DROP TABLE Clases;
DROP TABLE Clientes;
DROP TABLE Formacion_Profesores;
DROP TABLE Imparte_Clases;
DROP TABLE Inscripciones;
DROP TABLE Limpiadores;
DROP TABLE Profesores;
DROP TABLE Salas;
DROP TABLE Titulos;
DROP TABLE Trabajadores;

