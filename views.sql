CREATE OR REPLACE VIEW MIS_CLASES_DE_HOY AS
    SELECT t.nombre || ' ' || t.apellidos AS profesor, s.sala_id, s.nombre, cl.tipo, cl.especialidad, 
           ac.hora_inicio/100 || ':' || ac.hora_inicio%100 AS inicio,
           ac.hora_fin/100 || ':' || ac.hora_fin%100 AS fin,
           CASE WHEN ac.dia = 0 THEN 'Domingo'
                WHEN ac.dia = 1 THEN 'Lunes'
                WHEN ac.dia = 2 THEN 'Martes'
                WHEN ac.dia = 3 THEN 'Miercoles'
                WHEN ac.dia = 4 THEN 'Jueves'
                WHEN ac.dia = 5 THEN 'Viernes'
                WHEN ac.dia = 6 THEN 'Sabado'
            END AS duracion
    FROM CLIENTES AS c JOIN INSCRIPCIONES USING (cliente_id)
                       JOIN CLASES AS cl USING (clase_id)
                       JOIN IMPARTE_CLASES USING (clase_id)
                       JOIN TRABAJADORES AS t USING (trabajador_id)
                       JOIN AGENDA_CLASES AS ac USING (clase_id)
                       JOIN SALAS AS s USING (sala_id)
    WHERE c.user_id = CURRENT_USER AND ac.dia = EXTRACT(DOW FROM CURRENT_DATE);

CREATE OR REPLACE VIEW PLANIFICACION_SEMANAL_LIMPIEZA AS
SELECT CASE WHEN dia_semana = 0 THEN 'Domingo'
            WHEN dia_semana = 1 THEN 'Lunes'
            WHEN dia_semana = 2 THEN 'Martes'
            WHEN dia_semana = 3 THEN 'Miercoles'
            WHEN dia_semana = 4 THEN 'Jueves'
            WHEN dia_semana = 5 THEN 'Viernes'
            WHEN dia_semana = 6 THEN 'Sabado'
       END AS dia_semana,
       (t.nombre || ' ' || t.apellidos) AS limpiador, 
       s.sala_id AS numero_sala, s.nombre AS nombre_sala
FROM TRABAJADORES AS t JOIN LIMPIADORES USING (trabajador_id)
                       JOIN AGENDA_LIMPIEZA AS al USING (trabajador_id)
                       JOIN SALAS AS s USING (sala_id)
GROUP BY 1, 2, 3;

CREATE OR REPLACE VIEW ALUMNOS_DE_KARATE AS 
  SELECT  cli.nombre || ' ' || cli.apellidos AS alumno
  FROM CLIENTES AS cli JOIN INSCRIPCIONES USING (cliente_id)
                       JOIN CLASES AS cla USING (clase_id)
  WHERE LOWER(cla.nombre) = 'karate'
  GROUP BY cli.nombre, cli.apellidos;

-- Se repiten los alumnos si un alumno está suscrito a dos clases
-- Puede solucionarse utilizando un GROUP BY
CREATE OR REPLACE VIEW ALUMNOS_CLASE_HOY AS
  SELECT cli.nombre || ' ' || cli.apellidos AS alumno, cla.nombre AS clase
  FROM CLIENTES AS cli JOIN INSCRIPCIONES USING (cliente_id)  
                       JOIN CLASES AS cla USING (clase_id)
                       JOIN AGENDA_CLASES AS ac USING (clase_id)
                       JOIN IMPARTE_CLASES USING (clase_id)
                       JOIN PROFESORES USING (trabajador_id)
                       JOIN TRABAJADORES AS t USING (trabajador_id)
  WHERE t.user_id = CURRENT_USER AND ac.dia = EXTRACT (DOW FROM CURRENT_DATE);
