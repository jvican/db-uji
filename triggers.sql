DROP TRIGGER IF EXISTS compruebaHorarioClases ON INSCRIPCIONES;

-- Operaciones que deben fallar para este trigger
-- INSERT INTO INSCRIPCIONES VALUES (1,3, DATE '12-01-2015');
-- INSERT INTO INSCRIPCIONES VALUES (1,4, DATE '12-01-2015');
-- INSERT INTO INSCRIPCIONES VALUES (1,5, DATE '12-01-2015');
-- UPDATE INSCRIPCIONES SET clase_id = 5 WHERE cliente_id = 7 AND clase_id = 6;
-- UPDATE INSCRIPCIONES SET clase_id = 4 WHERE cliente_id = 7 AND clase_id = 6;
-- UPDATE INSCRIPCIONES SET clase_id = 3 WHERE cliente_id = 7 AND clase_id = 6;

CREATE OR REPLACE FUNCTION clasesMismoHorario() RETURNS TRIGGER AS $$
    DECLARE
        ini_hora_clase agenda_clases.hora_inicio%TYPE;
        fin_hora_clase agenda_clases.hora_fin%TYPE;
        dia_clase agenda_clases.dia%TYPE;
        cliente_id_ahora clientes.cliente_id%TYPE;
        clase_id_ahora clases.clase_id%TYPE;
        incumplen INTEGER;
    BEGIN
        -- Si se actualiza cliente_id, utiliza el nuevo
        IF TG_OP = 'INSERT' OR 
            (TG_OP = 'UPDATE' AND NEW.cliente_id != OLD.cliente_ID) THEN
            SELECT NEW.cliente_id INTO cliente_id_ahora;
        ELSE
            SELECT OLD.cliente_id INTO cliente_id_ahora;
        END IF;

        -- Si se actualiza clase_id, utiliza el nuevo
        IF TG_OP = 'INSERT' OR 
            (TG_OP = 'UPDATE' AND NEW.clase_id != OLD.clase_id) THEN
            SELECT NEW.clase_id INTO clase_id_ahora;
        ELSE
            SELECT OLD.clase_id INTO clase_id_ahora;
        END IF;

        -- Almacena horario de la clase con la que tenemos que comparar
        SELECT a.hora_inicio, a.hora_fin, a.dia 
        INTO ini_hora_clase, fin_hora_clase, dia_clase
        FROM AGENDA_CLASES AS a
        WHERE clase_id = clase_id_ahora;

        -- Cogemos cuantos incumplen la restriccion de horario
        SELECT COUNT(clase_id) INTO incumplen
        FROM INSCRIPCIONES JOIN CLASES USING (clase_id)
                           JOIN AGENDA_CLASES USING (clase_id)
        WHERE cliente_id = cliente_id_ahora AND 
              ((ini_hora_clase BETWEEN hora_inicio AND hora_fin) OR
               (fin_hora_clase BETWEEN hora_inicio AND hora_fin)) AND
              dia = dia_clase;

        -- Salta error cuando alguno incumple
        IF incumplen > 0 THEN
            -- Se incumple la logica de negocio
            RAISE EXCEPTION 'No puedes inscribirte a una clase que coincide con otra clase a la que estás inscrito.';
        END IF;

        RETURN NEW;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER compruebaHorarioClases
BEFORE INSERT OR UPDATE ON INSCRIPCIONES
FOR EACH ROW
EXECUTE PROCEDURE clasesMismoHorario();

DROP TRIGGER IF EXISTS claseIndividual ON INSCRIPCIONES;

-- Operaciones que deben fallar para este trigger
-- INSERT INTO INSCRIPCIONES VALUES (1, 7, DATE '15-02-2015')
-- UPDATE INSCRIPCIONES SET clase_id = 6 WHERE cliente_id = 5 AND clase_id = 5;

CREATE OR REPLACE FUNCTION compruebaClaseIndividual() RETURNS TRIGGER AS $$
    DECLARE 
        clase_id_ahora clases.clase_id%TYPE;
        tipo_clase clases.tipo%TYPE;
        asignado INTEGER;
    BEGIN
        -- Si se actualiza clase_id, utiliza el nuevo
        IF TG_OP = 'INSERT' OR 
            (TG_OP = 'UPDATE' AND NEW.clase_id != OLD.clase_id) THEN
            SELECT NEW.clase_id INTO clase_id_ahora;
        ELSE
            SELECT OLD.clase_id INTO clase_id_ahora;
        END IF;

        SELECT tipo
        INTO tipo_clase 
        FROM CLASES 
        WHERE clase_id = clase_id_ahora;

        IF (LOWER(tipo_clase) = 'individual') THEN
            -- Esta asignado?
            SELECT COUNT(cliente_id) 
            INTO asignado 
            FROM INSCRIPCIONES 
            WHERE clase_id = clase_id_ahora;

            IF (asignado = 1) THEN
                RAISE EXCEPTION 'No se puede asignar mas de un usuario a una clase individual';
            END IF;
        END IF;

        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER claseIndividual
BEFORE INSERT OR UPDATE ON INSCRIPCIONES
FOR EACH ROW 
EXECUTE PROCEDURE compruebaClaseIndividual();

DROP TRIGGER IF EXISTS compruebaSiMoroso ON CLIENTES;

-- Consulta que ejecuta el trigger
-- UPDATE CLIENTES SET moroso=true WHERE id_cliente=4;

CREATE OR REPLACE FUNCTION clienteMarcadoMoroso() RETURNS TRIGGER AS $$
	DECLARE
		esMoroso clientes.moroso%TYPE;
		cliente_id_ahora clientes.cliente_id%TYPE;
	BEGIN
		 -- Si se actualiza cliente_id, utiliza el nuevo
        IF (TG_OP = 'UPDATE' AND NEW.cliente_id != OLD.cliente_ID) THEN
            SELECT NEW.cliente_id INTO cliente_id_ahora;
        ELSE
            SELECT OLD.cliente_id INTO cliente_id_ahora;
        END IF;

        --Almacenamos si es moroso o no
        SELECT moroso INTO esMoroso
        FROM CLIENTES
        WHERE cliente_id = cliente_id_ahora;

        -- Hay que borrar las actividades en las que esta matriculado
        If (esMoroso) THEN
        	DELETE FROM INSCRIPCIONES WHERE cliente_id = cliente_id_ahora;
        END IF;

        RETURN NEW;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER compruebaSiMoroso
AFTER UPDATE ON CLIENTES
FOR EACH ROW
EXECUTE PROCEDURE clienteMarcadoMoroso();
