-- Foreign Keys
ALTER TABLE Agenda_Clases DROP CONSTRAINT Agenda_Clase;

ALTER TABLE Agenda_Limpieza DROP CONSTRAINT Agenda_Limpieza_Limpiador;

ALTER TABLE Agenda_Limpieza DROP CONSTRAINT Agenda_Limpieza_Sala;

ALTER TABLE Agenda_Clases DROP CONSTRAINT Agenda_Sala;

ALTER TABLE Inscripciones DROP CONSTRAINT Cliente_Inscripciones;

ALTER TABLE Formacion_Profesores DROP CONSTRAINT Formacion_Profesor_Profesor;

ALTER TABLE Imparte_Clases DROP CONSTRAINT Imparte_Clase_Clase;

ALTER TABLE Imparte_Clases DROP CONSTRAINT Imparte_Clase_Profesor;

ALTER TABLE Inscripciones DROP CONSTRAINT Inscripciones_Clase;

ALTER TABLE Limpiadores DROP CONSTRAINT Limpiador_Trabajador;

ALTER TABLE Profesores DROP CONSTRAINT Profesor_Trabajador;

ALTER TABLE Administradores DROP CONSTRAINT Table_13_Trabajador;

ALTER TABLE Formacion_Profesores DROP CONSTRAINT Titulo_Formacion_Profesor;

DROP VIEW MIS_CLASES_DE_HOY;
DROP VIEW PLANIFICACION_SEMANAL_LIMPIEZA;
DROP VIEW ALUMNOS_CLASE_HOY;
DROP VIEW ALUMNOS_DE_KARATE;

DROP TABLE Administradores;
DROP TABLE Agenda_Clases;
DROP TABLE Agenda_Limpieza;
DROP TABLE Clases;
DROP TABLE Clientes;
DROP TABLE Formacion_Profesores;
DROP TABLE Imparte_Clases;
DROP TABLE Inscripciones;
DROP TABLE Limpiadores;
DROP TABLE Profesores;
DROP TABLE Salas;
DROP TABLE Titulos;
DROP TABLE Trabajadores;

-- Table: Administradores
CREATE TABLE Administradores (
    trabajador_id int  NOT NULL,
    turno varchar(40)  NOT NULL,
    CONSTRAINT Administradores_pk PRIMARY KEY (trabajador_id)
);



-- Table: Agenda_Clases
CREATE TABLE Agenda_Clases (
    sala_id int  NOT NULL,
    clase_id int  NOT NULL,
    dia smallint  NOT NULL,
    hora_inicio smallint  NOT NULL,
    hora_fin smallint  NOT NULL,
    CONSTRAINT Agenda_Clases_pk PRIMARY KEY (sala_id,clase_id)
);



-- Table: Agenda_Limpieza
CREATE TABLE Agenda_Limpieza (
    trabajador_id int  NOT NULL,
    sala_id int  NOT NULL,
    dia_semana smallint  NOT NULL,
    CONSTRAINT Agenda_Limpieza_pk PRIMARY KEY (trabajador_id,sala_id,dia_semana)
);



-- Table: Clases
CREATE TABLE Clases (
    tipo varchar(20)  NOT NULL,
    matriculados int  NOT NULL,
    min int  NOT NULL,
    max int  NOT NULL,
    nombre varchar(20)  NOT NULL,
    clase_id int  NOT NULL,
    especialidad varchar(100)  NOT NULL,
    CONSTRAINT Clases_pk PRIMARY KEY (clase_id)
);



-- Table: Clientes
CREATE TABLE Clientes (
    dni varchar(9)  NOT NULL,
    nombre varchar(100)  NOT NULL,
    apellidos varchar(100)  NOT NULL,
    telefono int  NOT NULL,
    email varchar(100)  NOT NULL,
    fecha_alta date  NOT NULL,
    cuenta_bancaria varchar(24)  NOT NULL,
    moroso boolean  NOT NULL,
    poblacion varchar(200)  NOT NULL,
    cp int  NOT NULL,
    calle varchar(100)  NOT NULL,
    cliente_id int  NOT NULL,
    fecha_baja date  NULL,
    id_admin_baja int  NULL,
    user_id varchar(20)  NOT NULL,
    CONSTRAINT Clientes_User_Id UNIQUE (user_id) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT Clientes_pk PRIMARY KEY (cliente_id)
);



-- Table: Formacion_Profesores
CREATE TABLE Formacion_Profesores (
    trabajador_id int  NOT NULL,
    titulo_id int  NOT NULL,
    CONSTRAINT Formacion_Profesores_pk PRIMARY KEY (trabajador_id,titulo_id)
);



-- Table: Imparte_Clases
CREATE TABLE Imparte_Clases (
    trabajador_id int  NOT NULL,
    clase_id int  NOT NULL,
    CONSTRAINT Imparte_Clases_pk PRIMARY KEY (trabajador_id,clase_id)
);



-- Table: Inscripciones
CREATE TABLE Inscripciones (
    cliente_id int  NOT NULL,
    clase_id int  NOT NULL,
    fecha date  NOT NULL,
    CONSTRAINT Inscripciones_pk PRIMARY KEY (cliente_id,clase_id)
);



-- Table: Limpiadores
CREATE TABLE Limpiadores (
    trabajador_id int  NOT NULL,
    CONSTRAINT Limpiadores_pk PRIMARY KEY (trabajador_id)
);



-- Table: Profesores
CREATE TABLE Profesores (
    trabajador_id int  NOT NULL,
    horas_semana int  NOT NULL,
    CONSTRAINT Profesores_pk PRIMARY KEY (trabajador_id)
);



-- Table: Salas
CREATE TABLE Salas (
    sala_id int  NOT NULL,
    nombre varchar(100)  NOT NULL,
    tipo varchar(20)  NOT NULL,
    capacidad int  NOT NULL,
    CONSTRAINT Salas_pk PRIMARY KEY (sala_id)
);



-- Table: Titulos
CREATE TABLE Titulos (
    titulo_id int  NOT NULL,
    nombre varchar(100)  NOT NULL,
    descripcion text  NOT NULL,
    CONSTRAINT Titulos_pk PRIMARY KEY (titulo_id)
);



-- Table: Trabajadores
CREATE TABLE Trabajadores (
    dni varchar(9)  NOT NULL,
    nombre varchar(100)  NOT NULL,
    apellidos varchar(100)  NOT NULL,
    telefono int  NOT NULL,
    email varchar(100)  NOT NULL,
    calle varchar(200)  NOT NULL,
    cp int  NOT NULL,
    poblacion varchar(200)  NOT NULL,
    trabajador_id int  NOT NULL,
    user_id varchar(20)  NOT NULL,
    CONSTRAINT Trabajadores_user_id UNIQUE (user_id) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT Trabajadores_pk PRIMARY KEY (trabajador_id)
);







-- foreign keys
-- Reference:  Agenda_Clase (table: Agenda_Clases)

ALTER TABLE Agenda_Clases ADD CONSTRAINT Agenda_Clase 
    FOREIGN KEY (clase_id)
    REFERENCES Clases (clase_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Agenda_Limpieza_Limpiador (table: Agenda_Limpieza)

ALTER TABLE Agenda_Limpieza ADD CONSTRAINT Agenda_Limpieza_Limpiador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Limpiadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  RESTRICT 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Agenda_Limpieza_Sala (table: Agenda_Limpieza)

ALTER TABLE Agenda_Limpieza ADD CONSTRAINT Agenda_Limpieza_Sala 
    FOREIGN KEY (sala_id)
    REFERENCES Salas (sala_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Agenda_Sala (table: Agenda_Clases)

ALTER TABLE Agenda_Clases ADD CONSTRAINT Agenda_Sala 
    FOREIGN KEY (sala_id)
    REFERENCES Salas (sala_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Cliente_Inscripciones (table: Inscripciones)

ALTER TABLE Inscripciones ADD CONSTRAINT Cliente_Inscripciones 
    FOREIGN KEY (cliente_id)
    REFERENCES Clientes (cliente_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Formacion_Profesor_Profesor (table: Formacion_Profesores)

ALTER TABLE Formacion_Profesores ADD CONSTRAINT Formacion_Profesor_Profesor 
    FOREIGN KEY (trabajador_id)
    REFERENCES Profesores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Imparte_Clase_Clase (table: Imparte_Clases)

ALTER TABLE Imparte_Clases ADD CONSTRAINT Imparte_Clase_Clase 
    FOREIGN KEY (clase_id)
    REFERENCES Clases (clase_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Imparte_Clase_Profesor (table: Imparte_Clases)

ALTER TABLE Imparte_Clases ADD CONSTRAINT Imparte_Clase_Profesor 
    FOREIGN KEY (trabajador_id)
    REFERENCES Profesores (trabajador_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Inscripciones_Clase (table: Inscripciones)

ALTER TABLE Inscripciones ADD CONSTRAINT Inscripciones_Clase 
    FOREIGN KEY (clase_id)
    REFERENCES Clases (clase_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Limpiador_Trabajador (table: Limpiadores)

ALTER TABLE Limpiadores ADD CONSTRAINT Limpiador_Trabajador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Trabajadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Profesor_Trabajador (table: Profesores)

ALTER TABLE Profesores ADD CONSTRAINT Profesor_Trabajador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Trabajadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Table_13_Trabajador (table: Administradores)

ALTER TABLE Administradores ADD CONSTRAINT Table_13_Trabajador 
    FOREIGN KEY (trabajador_id)
    REFERENCES Trabajadores (trabajador_id)
    ON DELETE  RESTRICT 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;

-- Reference:  Titulo_Formacion_Profesor (table: Formacion_Profesores)

ALTER TABLE Formacion_Profesores ADD CONSTRAINT Titulo_Formacion_Profesor 
    FOREIGN KEY (titulo_id)
    REFERENCES Titulos (titulo_id)
    ON DELETE  CASCADE 
    ON UPDATE  CASCADE 
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE 
;
DROP TRIGGER IF EXISTS compruebaHorarioClases ON INSCRIPCIONES;

-- Consultas que deben fallar para este trigger
-- INSERT INTO INSCRIPCIONES VALUES (1,3, DATE '12-01-2015');
-- INSERT INTO INSCRIPCIONES VALUES (1,4, DATE '12-01-2015');
-- INSERT INTO INSCRIPCIONES VALUES (1,5, DATE '12-01-2015');
-- UPDATE INSCRIPCIONES SET clase_id = 5 WHERE cliente_id = 7 AND clase_id = 6;
-- UPDATE INSCRIPCIONES SET clase_id = 4 WHERE cliente_id = 7 AND clase_id = 6;
-- UPDATE INSCRIPCIONES SET clase_id = 3 WHERE cliente_id = 7 AND clase_id = 6;

CREATE OR REPLACE FUNCTION clasesMismoHorario() RETURNS TRIGGER AS $$
    DECLARE
        ini_hora_clase agenda_clases.hora_inicio%TYPE;
        fin_hora_clase agenda_clases.hora_fin%TYPE;
        dia_clase agenda_clases.dia%TYPE;
        cliente_id_ahora clientes.cliente_id%TYPE;
        clase_id_ahora clases.clase_id%TYPE;
        incumplen INTEGER;
    BEGIN
        -- Si se actualiza cliente_id, utiliza el nuevo
        IF TG_OP = 'INSERT' OR 
            (TG_OP = 'UPDATE' AND NEW.cliente_id != OLD.cliente_ID) THEN
            SELECT NEW.cliente_id INTO cliente_id_ahora;
        ELSE
            SELECT OLD.cliente_id INTO cliente_id_ahora;
        END IF;

        -- Si se actualiza clase_id, utiliza el nuevo
        IF TG_OP = 'INSERT' OR 
            (TG_OP = 'UPDATE' AND NEW.clase_id != OLD.clase_id) THEN
            SELECT NEW.clase_id INTO clase_id_ahora;
        ELSE
            SELECT OLD.clase_id INTO clase_id_ahora;
        END IF;

        -- Almacena horario de la clase con la que tenemos que comparar
        SELECT a.hora_inicio, a.hora_fin, a.dia 
        INTO ini_hora_clase, fin_hora_clase, dia_clase
        FROM AGENDA_CLASES AS a
        WHERE clase_id = clase_id_ahora;

        -- Cogemos cuantos incumplen la restriccion de horario
        SELECT COUNT(clase_id) INTO incumplen
        FROM INSCRIPCIONES JOIN CLASES USING (clase_id)
                           JOIN AGENDA_CLASES USING (clase_id)
        WHERE cliente_id = cliente_id_ahora AND 
              ((ini_hora_clase BETWEEN hora_inicio AND hora_fin) OR
               (fin_hora_clase BETWEEN hora_inicio AND hora_fin)) AND
              dia = dia_clase;

        -- Salta error cuando alguno incumple
        IF incumplen > 0 THEN
            -- Se incumple la logica de negocio
            RAISE EXCEPTION 'No puedes inscribirte a una clase que coincide con otra clase a la que estás inscrito.';
        END IF;

        RETURN NEW;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER compruebaHorarioClases
BEFORE INSERT OR UPDATE ON INSCRIPCIONES
FOR EACH ROW
EXECUTE PROCEDURE clasesMismoHorario();

DROP TRIGGER IF EXISTS claseIndividual ON INSCRIPCIONES;

-- Consultas que deben fallar para este trigger
-- INSERT INTO INSCRIPCIONES VALUES (1, 7, DATE '15-02-2015')
-- UPDATE INSCRIPCIONES SET clase_id = 6 WHERE cliente_id = 5 AND clase_id = 5;

CREATE OR REPLACE FUNCTION compruebaClaseIndividual() RETURNS TRIGGER AS $$
    DECLARE 
        clase_id_ahora clases.clase_id%TYPE;
        tipo_clase clases.tipo%TYPE;
        asignado INTEGER;
    BEGIN
        -- Si se actualiza clase_id, utiliza el nuevo
        IF TG_OP = 'INSERT' OR 
            (TG_OP = 'UPDATE' AND NEW.clase_id != OLD.clase_id) THEN
            SELECT NEW.clase_id INTO clase_id_ahora;
        ELSE
            SELECT OLD.clase_id INTO clase_id_ahora;
        END IF;

        SELECT tipo
        INTO tipo_clase 
        FROM CLASES 
        WHERE clase_id = clase_id_ahora;

        IF (LOWER(tipo_clase) = 'individual') THEN
            -- Esta asignado?
            SELECT COUNT(cliente_id) 
            INTO asignado 
            FROM INSCRIPCIONES 
            WHERE clase_id = clase_id_ahora;

            IF (asignado = 1) THEN
                RAISE EXCEPTION 'No se puede asignar mas de un usuario a una clase individual';
            END IF;
        END IF;

        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER claseIndividual
BEFORE INSERT OR UPDATE ON INSCRIPCIONES
FOR EACH ROW 
EXECUTE PROCEDURE compruebaClaseIndividual();

DROP TRIGGER IF EXISTS compruebaSiMoroso ON CLIENTES;

-- Consultas que comprueban el trigger
-- INSERT INTO INSCRIPCIONES VALUES (4,3, DATE '12-03-2015');
-- INSERT INTO INSCRIPCIONES VALUES (4,4, DATE '12-02-2015');
-- INSERT INTO INSCRIPCIONES VALUES (4,2, DATE '12-01-2015');
-- UPDATE CLIENTES SET moroso=true WHERE id_cliente=4;

CREATE OR REPLACE FUNCTION clienteMarcadoMoroso() RETURNS TRIGGER AS $$
	DECLARE
		esMoroso clientes.moroso%TYPE;
		cliente_id_ahora clientes.cliente_id%TYPE;
	BEGIN
		 -- Si se actualiza cliente_id, utiliza el nuevo
        IF (TG_OP = 'UPDATE' AND NEW.cliente_id != OLD.cliente_ID) THEN
            SELECT NEW.cliente_id INTO cliente_id_ahora;
        ELSE
            SELECT OLD.cliente_id INTO cliente_id_ahora;
        END IF;

        --Almacenamos si es moroso o no
        SELECT moroso INTO esMoroso
        FROM CLIENTES
        WHERE cliente_id = cliente_id_ahora;

        -- Hay que borrar las actividades en las que esta matriculado
        If (esMoroso) THEN
        	DELETE FROM INSCRIPCIONES WHERE cliente_id = cliente_id_ahora;
        END IF;

        RETURN NEW;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER compruebaSiMoroso
AFTER UPDATE ON CLIENTES
FOR EACH ROW
EXECUTE PROCEDURE clienteMarcadoMoroso();
CREATE OR REPLACE VIEW MIS_CLASES_DE_HOY AS
    SELECT t.nombre || ' ' || t.apellidos AS profesor, s.sala_id, s.nombre, cl.tipo, cl.especialidad, 
           ac.hora_inicio/100 || ':' || ac.hora_inicio%100 AS inicio,
           ac.hora_fin/100 || ':' || ac.hora_fin%100 AS fin,
           CASE WHEN ac.dia = 0 THEN 'Domingo'
                WHEN ac.dia = 1 THEN 'Lunes'
                WHEN ac.dia = 2 THEN 'Martes'
                WHEN ac.dia = 3 THEN 'Miercoles'
                WHEN ac.dia = 4 THEN 'Jueves'
                WHEN ac.dia = 5 THEN 'Viernes'
                WHEN ac.dia = 6 THEN 'Sabado'
            END AS duracion
    FROM CLIENTES AS c JOIN INSCRIPCIONES USING (cliente_id)
                       JOIN CLASES AS cl USING (clase_id)
                       JOIN IMPARTE_CLASES USING (clase_id)
                       JOIN TRABAJADORES AS t USING (trabajador_id)
                       JOIN AGENDA_CLASES AS ac USING (clase_id)
                       JOIN SALAS AS s USING (sala_id)
    WHERE c.user_id = CURRENT_USER AND ac.dia = EXTRACT(DOW FROM CURRENT_DATE);

CREATE OR REPLACE VIEW PLANIFICACION_SEMANAL_LIMPIEZA AS
SELECT CASE WHEN dia_semana = 0 THEN 'Domingo'
            WHEN dia_semana = 1 THEN 'Lunes'
            WHEN dia_semana = 2 THEN 'Martes'
            WHEN dia_semana = 3 THEN 'Miercoles'
            WHEN dia_semana = 4 THEN 'Jueves'
            WHEN dia_semana = 5 THEN 'Viernes'
            WHEN dia_semana = 6 THEN 'Sabado'
       END AS dia_semana,
       (t.nombre || ' ' || t.apellidos) AS limpiador, 
       s.sala_id AS numero_sala, s.nombre AS nombre_sala
FROM TRABAJADORES AS t JOIN LIMPIADORES USING (trabajador_id)
                       JOIN AGENDA_LIMPIEZA AS al USING (trabajador_id)
                       JOIN SALAS AS s USING (sala_id)
GROUP BY 1, 2, 3;

CREATE OR REPLACE VIEW ALUMNOS_DE_KARATE AS 
  SELECT  cli.nombre || ' ' || cli.apellidos
  FROM CLIENTES AS cli JOIN INSCRIPCIONES USING (cliente_id)
                       JOIN CLASES AS cla USING (clase_id)
  WHERE LOWER(cla.nombre) = 'karate'
  GROUP BY cli.nombre, cli.apellidos;

-- Se repiten los alumnos si un alumno está suscrito a dos clases
-- Puede solucionarse utilizando un GROUP BY
CREATE OR REPLACE VIEW ALUMNOS_CLASE_HOY AS
  SELECT cli.nombre || ' ' || cli.apellidos AS alumno, cla.nombre AS clase
  FROM CLIENTES AS cli JOIN INSCRIPCIONES USING (cliente_id)  
                       JOIN CLASES AS cla USING (clase_id)
                       JOIN AGENDA_CLASES AS ac USING (clase_id)
                       JOIN IMPARTE_CLASES USING (clase_id)
                       JOIN PROFESORES USING (trabajador_id)
                       JOIN TRABAJADORES AS t USING (trabajador_id)
  WHERE t.user_id = CURRENT_USER AND ac.dia = EXTRACT (DOW FROM CURRENT_DATE);
INSERT INTO TRABAJADORES VALUES ('12345678L', 'Raul', 'Gonzalez', 654987321, 'rgon@uji.es', 'C/Tomate', 12001, 'Castellon', 1, 'rgon');
INSERT INTO TRABAJADORES VALUES ('23456781L', 'Daniela', 'Vicente', 645123789, 'dvic@uji.es', 'C/Tortilla', 12002, 'Castellon', 2, 'dvic');
INSERT INTO TRABAJADORES VALUES ('34567812L', 'Carlos', 'Filip', 678912345, 'cfil@uji.es', 'C/Cuadro Picasso', 12003, 'Castellon', 3, 'cfil');
-- Utiliza al259374 para poder ver resultados
INSERT INTO TRABAJADORES VALUES ('45678123L', 'Jorge', 'Vicente Cantero', 691234578, 'lcla@uji.es', 'C/Esquina', 12004, 'Castellon', 4, 'al259374');
INSERT INTO TRABAJADORES VALUES ('45578123L', 'Ramon', 'Vilar', 611344234, 'rvil@uji.es', 'C/Pan rustico', 12005, 'Castellon', 5, 'rvil');
INSERT INTO TRABAJADORES VALUES ('56781234L', 'Laura', 'Llorens', 697774578, 'lllo@uji.es', 'C/Alcocebre', 12006, 'Castellon', 6, 'lllo');
INSERT INTO TRABAJADORES VALUES ('67812345L', 'Jesus', 'Ormaechea', 711231498, 'jbar@uji.es', 'C/Pimiento', 12007, 'Castellon', 7, 'jbar');
INSERT INTO TRABAJADORES VALUES ('78123456L', 'Esther', 'Avila', 675231458, 'eavi@uji.es', 'C/Cebolla', 12008, 'Castellon', 8, 'eavi');
 
INSERT INTO ADMINISTRADORES VALUES (1, 'manana'); 
INSERT INTO ADMINISTRADORES VALUES (2, 'fin de semana'); 
INSERT INTO ADMINISTRADORES VALUES (3, 'tarde'); 
 
INSERT INTO PROFESORES VALUES (4, 40); 
INSERT INTO PROFESORES VALUES (5, 20); 
INSERT INTO PROFESORES VALUES (6, 30); 
 
INSERT INTO TITULOS VALUES (1, 'Tecnico Superior en Animacion y Actividades Fisicas de relajacion', 'El monitor sabe ensenar a relajarse.'); 
INSERT INTO TITULOS VALUES (2, 'Tecnico en Actividades Deportivas de lucha', 'El monitor sabe luchar.'); 
INSERT INTO TITULOS VALUES (3, 'Monitor deportivo y musculacion', 'El monitor sabe entrenar y ensenar a sacar musculo.'); 
 
INSERT INTO FORMACION_PROFESORES VALUES (4, 1); 
INSERT INTO FORMACION_PROFESORES VALUES (5, 2); 
INSERT INTO FORMACION_PROFESORES VALUES (6, 3); 
 
INSERT INTO LIMPIADORES VALUES (7); 
INSERT INTO LIMPIADORES VALUES (8); 

INSERT INTO CLIENTES VALUES ('89120450P', 'Alvaro', 'Beltran', 745123454, 'abel@gmail.com', DATE '2015-06-01', 'ES4444123400993310204445', false, 'Castellon', 12015, 'C/Bigotes', 1, NULL, NULL, 'abel');
INSERT INTO CLIENTES VALUES ('19123497P', 'Manuela', 'Garcia', 745123777, 'mgar@gmail.com', DATE '2015-10-20', 'ES2720101409493310204446', false, 'Castellon', 12016, 'C/Mayor', 2, NULL, NULL, 'mgar');
-- Utiliza al259374 para poder ver resultados
INSERT INTO CLIENTES VALUES ('34110858S', 'Gaspar', 'Cantero', 745123655, 'gcant@gmail.com', DATE '2015-10-16', 'ES2720123499661310004477', true, 'Castellon', 12017, 'C/Central', 3, NULL, NULL, 'al259374');
INSERT INTO CLIENTES VALUES ('82153333G', 'Dario', 'Perez', 745123484, 'dper@gmail.com', DATE '2015-10-09', 'ES8880123444222878814488', false, 'Castellon', 12018, 'C/Estrada', 4, NULL, NULL, 'dper');
INSERT INTO CLIENTES VALUES ('71143169H', 'Maria', 'Lopez', 745113469, 'mlop@gmail.com', DATE '2015-06-15', 'ES4560123499993347843399', false, 'Castellon', 12019, 'C/Merluza', 5, NULL, NULL, 'mlop');
INSERT INTO CLIENTES VALUES ('30033349J', 'Eduardo', 'Morcillo', 745122469, 'emor@gmail.com', DATE '2015-01-14', 'ES2720123499993330042399', false, 'Castellon', 12020, 'C/Emperador', 6, DATE '2015-10-01', 1, 'emor');
INSERT INTO CLIENTES VALUES ('42124412K', 'Jesus', 'Cebrian', 745127469, 'jceb@gmail.com', DATE '2015-02-13', 'ES2720123499993330076595', false, 'Castellon', 12021, 'C/Salmonete', 7, DATE '2015-10-02', 2, 'jceb');
 
INSERT INTO SALAS VALUES (1, 'Sala 1', 'Tatami', 40); 
INSERT INTO SALAS VALUES (2, 'Sala 2', 'Pesas', 60); 
INSERT INTO SALAS VALUES (3, 'Sala 3', 'Multifuncional', 70); 
INSERT INTO SALAS VALUES (4, 'Sala 4', 'General', 70); 
INSERT INTO SALAS VALUES (5, 'Sala 5', 'General', 70); 

INSERT INTO AGENDA_LIMPIEZA VALUES (7, 1, 1);
INSERT INTO AGENDA_LIMPIEZA VALUES (7, 2, 2);
INSERT INTO AGENDA_LIMPIEZA VALUES (7, 3, 4);
INSERT INTO AGENDA_LIMPIEZA VALUES (7, 4, 5);
INSERT INTO AGENDA_LIMPIEZA VALUES (8, 5, 1);
INSERT INTO AGENDA_LIMPIEZA VALUES (8, 4, 2);
INSERT INTO AGENDA_LIMPIEZA VALUES (8, 1, 4);
 
INSERT INTO CLASES VALUES ('Grupal', 2, 2, 15, 'Karate', 1, 'Realizacion de Karate'); 
INSERT INTO CLASES VALUES ('Grupal', 2, 6, 20, 'Judo', 2, 'Realizacion de Judo'); 
INSERT INTO CLASES VALUES ('Grupal', 1, 20, 30, 'Yoga', 3, 'Realizacion de Yoga'); 
INSERT INTO CLASES VALUES ('Grupal', 10, 10, 20, 'Aerobic', 4, 'Realizacion de Aerobic'); 
INSERT INTO CLASES VALUES ('Grupal', 1, 30, 50, 'Pesas', 5, 'Musculacion'); 
INSERT INTO CLASES VALUES ('Individual', 1, 1, 1, 'Entrenamiento', 6, 'Entrenamiento personal'); 
INSERT INTO CLASES VALUES ('Grupal', 0, 40, 60, 'Entrenamiento', 7, 'Entrenamiento personal'); 
 
INSERT INTO AGENDA_CLASES VALUES (1, 1, 2, 1800, 2000); 
INSERT INTO AGENDA_CLASES VALUES (2, 3, 2, 1800, 2000); 
INSERT INTO AGENDA_CLASES VALUES (4, 5, 2, 1500, 1900); 
INSERT INTO AGENDA_CLASES VALUES (4, 4, 2, 1900, 2100); 
INSERT INTO AGENDA_CLASES VALUES (2, 5, 2, 1500, 2000); 
INSERT INTO AGENDA_CLASES VALUES (1, 2, 1, 1700, 2100); 
INSERT INTO AGENDA_CLASES VALUES (3, 3, 4, 1600, 1730); 
INSERT INTO AGENDA_CLASES VALUES (5, 5, 5, 0900, 1100); 
INSERT INTO AGENDA_CLASES VALUES (3, 5, 4, 1800, 2000); 
 
INSERT INTO IMPARTE_CLASES VALUES (4, 3); 
INSERT INTO IMPARTE_CLASES VALUES (4, 4); 
INSERT INTO IMPARTE_CLASES VALUES (4, 7); 
INSERT INTO IMPARTE_CLASES VALUES (5, 1); 
INSERT INTO IMPARTE_CLASES VALUES (5, 2); 
INSERT INTO IMPARTE_CLASES VALUES (6, 5); 
 
INSERT INTO INSCRIPCIONES VALUES (1, 1, DATE '2015-06-01'); 
INSERT INTO INSCRIPCIONES VALUES (2, 1, DATE '2015-10-20'); 
INSERT INTO INSCRIPCIONES VALUES (3, 3, DATE '2015-10-16'); 
INSERT INTO INSCRIPCIONES VALUES (4, 5, DATE '2015-10-09'); 
INSERT INTO INSCRIPCIONES VALUES (5, 5, DATE '2015-06-15'); 
INSERT INTO INSCRIPCIONES VALUES (3, 6, DATE '2015-06-15'); 
INSERT INTO INSCRIPCIONES VALUES (7, 1, DATE '2015-06-15'); 
INSERT INTO INSCRIPCIONES VALUES (7, 7, DATE '2015-06-15'); 
